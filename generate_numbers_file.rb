#!/usr/bin/env ruby

def bytes_to_gigabytes(num_bytes)
  ratio = 2**30
  num_bytes.to_f / ratio.to_f
end

def generate_some_numbers(filename)
  File.open(filename, 'a') do |f|
    100.times do
      f << "#{rand}\n"
    end
  end
end

max_size_in_gigabytes = ARGV[0].to_f
filename = "#{max_size_in_gigabytes}GB_numbers"

File.open(filename, 'w+') unless File.exist?(filename)

while bytes_to_gigabytes(File.stat(filename).size) < max_size_in_gigabytes
  generate_some_numbers(filename)
end
