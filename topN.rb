#!/usr/bin/env ruby

# Write a program, topN, that given a number N and an arbitrarily large file that contains individual numbers on each line (e.g. 200Gb file), will output the largest N numbers, highest first. Tell me about the run time/space complexity of it, and whether you think there's room for improvement in your approach.

def topN(n, filename)
  top_numbers = []
  File.open(filename, 'r').each do |line|
    current_number = line.to_f
    if top_numbers.length < n
      top_numbers << current_number
    else
      min_element = top_numbers.min
      if current_number > min_element
        top_numbers[top_numbers.index(min_element)] = current_number
      end
    end
  end

  top_numbers.sort.reverse
end

n = ARGV[0].to_i
filename = ARGV[1]

puts topN(n, filename)
